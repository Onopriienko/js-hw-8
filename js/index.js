document.body.insertAdjacentHTML("afterbegin",`<div id='main-container'><p id="name">price</p><form><input type="text" id='input' placeholder="Enter Price"></form></div>`);
const mainContainer = document.getElementById("main-container");
const input = document.getElementById("input");
mainContainer.addEventListener("focus",() => input.classList.add('input-border-green'),true);

input.onblur = function () {
    if (isNaN(input.value) || input.value <= 0){
        input.classList.add('input-border-red');
        mainContainer.insertAdjacentHTML("beforeend",`<span id="false-text">Please enter correct price</span>`);
    }else {
        mainContainer.insertAdjacentHTML("afterbegin",`<div id="box-for-success"><span id="success-text">Текущая цена: ${input.value}</span><button id="clear-all">X</button></div>`);
        document.getElementById("clear-all").addEventListener("click",clear,true);
        input.classList.add('input-green-text');
    }
};
input.onfocus = function () {
    if (this.classList.contains('input-border-red')){
        this.classList.remove('input-border-red');
        document.getElementById("false-text").remove();
    }
};
function clear(){
    document.getElementById("success-text").remove();
    this.remove();
    document.getElementById("box-for-success").remove();
    input.value = '';
    if (input.classList.contains('input-green-text')){
        input.classList.remove('input-green-text')
    }
}


